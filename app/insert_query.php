<?php
//require 'connect.php';
//Selecting multiple rows using prepared statements.
$sql = "SELECT `id`, `make`, `model` FROM `cars` WHERE `make` = :make";

//Prepare our SELECT statement.
$statement = $pdo->prepare($sql);

//The make that we are looking for.
$make = 'Nissan';

//Bind our value to the paramater :make.
$statement->bindValue(':make', $make);

//Execute our statement.
$statement->execute();

//Fetch our rows. Array (empty if no rows). False on failure.
$rows = $statement->fetchAll(PDO::FETCH_ASSOC);

//Loop through the $rows array.
foreach($rows as $row){
	echo 'Make: ' . $row['make'] . '<br>';
	echo 'Model: ' . $row['model'] . '<br>';
}
?>
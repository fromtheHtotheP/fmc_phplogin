#!/usr/bin/env bash

apt-get update
debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password server'
debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again server'
apt-get -f install
apt-get install -y apache2 
 
#Install mysql-server
echo 'mysql-server mysql-server/root_password password root' | debconf-set-selections
echo 'mysql-server mysql-server/root_password_again password root' | debconf-set-selections
apt-get install -y mysql-server  


# add php7.0
apt-get install python-software-properties software-properties-common -y
LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php
apt-get install php7.0 -y
apt-get install php7.0-fpm -y
apt-get install php7.0-mysql -y
phpenmod pdo_mysql

# add composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

# add git
apt-get install git-all -y

